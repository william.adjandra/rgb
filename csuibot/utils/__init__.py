from csuibot.utils import zodiac as z
import re
import requests
from bs4 import BeautifulSoup
import http.client, urllib.request, urllib.parse, urllib.error, base64
import json


def lookup_celeb(file):
    headers = {
        # Request headers. Replace the key below with your subscription key.
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': '051a4e37a0664f5a9695b630c847c30a',
    }

    params = urllib.parse.urlencode({
        # Request parameters. The language setting "unk" means automatically detect the language.
        'visualFeatures': 'Categories',
        'details': 'Celebrities',
        'language': 'en',
    })

    # Replace the three dots below with the URL of a JPEG image containing text.
    body = "{'url':'" + file + "'}"

    conn = http.client.HTTPSConnection('southeastasia.api.cognitive.microsoft.com')
    conn.request("POST", "/vision/v1.0/analyze?%s" % params, body, headers)
    response = conn.getresponse()
    data = response.read()
    dataJson = json.loads(data.decode('utf-8'))
    if(dataJson['categories'][0]['name'] == 'people_'):
        name = dataJson['categories'][0]['detail']['celebrities'][0]['name']
        confidence = dataJson['categories'][0]['detail']['celebrities'][0]['confidence']
        confidence = str(confidence)
    else:
        name = dataJson['categories'][1]['detail']['celebrities'][0]['name']
        confidence = dataJson['categories'][1]['detail']['celebrities'][0]['confidence']
        confidence = str(confidence)

    output = name + " (" + confidence + ")"
    

    return output


def lookup_colour(string):
    url = 'http://www.colourlovers.com/api/color/' + string
    response = requests.get(url)

    html = response.text
    soup = BeautifulSoup(html, "html.parser")
    r = soup.find('red').text
    g = soup.find('green').text
    b = soup.find('blue').text
    return '(' + r + ',' + g + ',' + b + ')'


def lookup_billboard_hotcountry(artist):
    url = 'http://www.billboard.com/rss/charts/country-songs'
    artist = str.lower(artist)
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html, "html.parser")
    regex = re.compile('.*' + artist + '.*')
    isKetemu = False
    for tag in soup.find_all('item'):
        artist_name = tag.find('artist').text
        song = tag.find('chart_item_title').text
        position = tag.find('rank_this_week').text
        if(regex.match(str.lower(artist_name))):
            output = (artist_name + "\n" + song + "\n" + position)
            isKetemu = True
    if(isKetemu is False):
        output = artist + " is not on the list"
    return output


def lookup_billboard_100hot():
    url = 'http://www.billboard.com/rss/charts/hot-100'
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html, "html.parser")
    output = ""
    limit = 50
    for tag in soup.find_all('item'):
        artist_name = tag.find('artist').text
        song = tag.find('chart_item_title').text
        position = tag.find('rank_this_week').text
        text = position + ". " + song + "\n" + artist_name + "\n"
        output = output + text
        limit -= 1
        if(limit == 0):
            break
    return output


def lookup_extract_handwritter():
    headers = {
        # Request headers. Replace the key below with your subscription key.
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': '13hc77781f7e4b19b5fcdd72a8df7156',
    }

    params = urllib.parse.urlencode({
        # Request parameters. The language setting "unk" means automatically detect the language.
        'language': 'unk',
        'detectOrientation ': 'true',
    })

    # Replace the three dots below with the URL of a JPEG image containing text.
    body = "{'url':'...'}"

    try:
        conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
        conn.request("POST", "/vision/v1.0/ocr?%s" % params, body, headers)
        response = conn.getresponse()
        data = response.read()
        print(data)
        conn.close()
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))


def lookup_zodiac(month, day):
    zodiacs = [
        z.Aries(),
        z.Taurus(),
        z.Gemini(),
        z.Cancer(),
        z.Leo(),
        z.Virgo(),
        z.Libra(),
        z.Scorpio(),
        z.Sagittarius(),
        z.Capricorn(),
        z.Aquarius(),
        z.Pisces()
    ]

    for zodiac in zodiacs:
        if zodiac.date_includes(month, day):
            return zodiac.name
    else:
        return 'Unknown zodiac'


def lookup_chinese_zodiac(year):
    num_zodiacs = 12
    zodiacs = {
        0: 'rat',
        1: 'buffalo',
        2: 'tiger',
        3: 'rabbit',
        4: 'dragon',
        5: 'snake',
        6: 'horse',
        7: 'goat',
        8: 'monkey'
    }
    ix = (year - 4) % num_zodiacs

    try:
        return zodiacs[ix]
    except KeyError:
        return 'Unknown zodiac'
