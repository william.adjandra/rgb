from . import app, bot
from .utils import lookup_zodiac, lookup_chinese_zodiac, lookup_colour
from .utils import lookup_billboard_hotcountry, lookup_billboard_100hot
from .utils import lookup_celeb


@bot.message_handler(regexp=r'^/about$')
def help(message):
    app.logger.debug("'about' command detected")
    about_text = (
        'CSUIBot v0.0.1\n\n'
        'Dari Fasilkom, oleh Fasilkom, untuk Fasilkom!'
    )
    bot.reply_to(message, about_text)


@bot.message_handler(content_types=['photo'], func=lambda message: message.caption is None)
@bot.message_handler(content_types=['photo'], func=lambda message: message.caption == 'is_celeb')
@bot.message_handler(content_types=['photo'], func=lambda message: message.caption == 'Is_celeb')
@bot.message_handler(regexp=r'^(/)?[Ii]s_celeb$')
def crop_failed(message):
    bot.reply_to(message, 'Send me an image and caption it with /is_celeb')


@bot.message_handler(content_types=['photo'], func=lambda message: message.caption == '/is_celeb')
def well_known_celebrity(message):
    app.logger.debug("'is_celeb' command detected")
    API_TOKEN = '335997066:AAEsRl6xjliYdYaPP4D6_5i64H6fVQb2fs4'
    file_info = bot.get_file(message.photo[len(message.photo)-1].file_id)
    downloaded_file = (
                        'https://api.telegram.org/file/bot{0}/{1}'
                        .format(API_TOKEN, file_info.file_path))

    hasil = lookup_celeb(downloaded_file)
    bot.reply_to(message, hasil)


@bot.message_handler(regexp=r'.*[Ff][Aa][Ss][Ii][Ll][Kk][Oo][Mm].*$')
def viva_fasilkom(message):
    app.logger.debug("'viva_fasilkom' command detected")

    string = ('Viva, Viva, Viva Fasilkom')
    bot.reply_to(message, string)


@bot.message_handler(regexp=r'^/colour #[0-9a-wA-W]{6}$')
def colour(message):
    app.logger.debug("'colour' command detected")
    hexadecimal = message.text[-6:]
    try:
        int(hexadecimal[:2], 16)
        int(hexadecimal[2:4], 16)
        int(hexadecimal[-2:], 16)
        rgb = lookup_colour(hexadecimal)
    except ValueError:
        bot.reply_to(message, 'Hex Colour Invalid')
    else:
        bot.reply_to(message, rgb)


@bot.message_handler(regexp=r'^/billboard hotcountry .*$')
def detect(message):
    app.logger.debug("'billboard hotcountry' command detected")
    artist = message.text[22:]
    description = lookup_billboard_hotcountry(artist)
    bot.reply_to(message, description)


@bot.message_handler(regexp=r'^/billboard hot100$')
def billboard_hot_100(message):
    app.logger.debug("'billboard hotcountry' command detected")
    description = lookup_billboard_100hot()
    bot.reply_to(message, description)


@bot.message_handler(regexp=r'^/zodiac \d{4}\-\d{2}\-\d{2}$')
def zodiac(message):
    app.logger.debug("'zodiac' command detected")
    _, date_str = message.text.split(' ')
    _, month, day = parse_date(date_str)
    app.logger.debug('month = {}, day = {}'.format(month, day))

    try:
        zodiac = lookup_zodiac(month, day)
    except ValueError:
        bot.reply_to(message, 'Month or day is invalid')
    else:
        bot.reply_to(message, zodiac)


@bot.message_handler(regexp=r'^/shio \d{4}\-\d{2}\-\d{2}$')
def shio(message):
    app.logger.debug("'shio' command detected")
    _, date_str = message.text.split(' ')
    year, _, _ = parse_date(date_str)
    app.logger.debug('year = {}'.format(year))

    try:
        zodiac = lookup_chinese_zodiac(year)
    except ValueError:
        bot.reply_to(message, 'Year is invalid')
    else:
        bot.reply_to(message, zodiac)


def parse_date(text):
    return tuple(map(int, text.split('-')))
