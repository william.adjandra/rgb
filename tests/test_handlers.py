from unittest.mock import Mock

from csuibot.handlers import help, zodiac, shio, colour, viva_fasilkom
from csuibot.handlers import detect, well_known_celebrity, crop_failed
from csuibot.utils import lookup_celeb

def test_celeb(mocker):
    celeb = lookup_celeb('http://ksassets.timeincuk.net/wp/uploads/sites/46/2016/05/Ryan-Reynolds-920x518.jpg')

    expect = 'Ryan Reynolds (0.944896638)'
    
    assert celeb == expect


def test_cropf1(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock(text='is_celeb')

    crop_failed(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = ('Send me an image and caption it with /is_celeb')
    assert args[1] == expected_text


def test_billboard_hotcountry(mocker):
    fake_colour = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_billboard_hotcountry', return_value=fake_colour)
    mock_message = Mock(text='/billboard hotcountry jason aldean')

    detect(mock_message)
    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_colour


def test_billboard_hotcountry2(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock(text='/billboard hotcountry josh turner')

    detect(mock_message)
    string = ("Josh Turner\nHometown Girl\n7")
    args, _ = mocked_reply_to.call_args
    assert args[1] == string


def test_viva_fasilkom(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock(text='fasilkom paling oke')

    viva_fasilkom(mock_message)
    string = ('Viva, Viva, Viva Fasilkom')
    args, _ = mocked_reply_to.call_args
    assert args[1] == string


def test_colour(mocker):
    fake_colour = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_colour', return_value=fake_colour)
    mock_message = Mock(text='/colour #9817Ab')

    colour(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_colour


def test_colour_invalid_hex(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_colour', side_effect=ValueError)
    mock_message = Mock(text='/colour #98Ya9O')

    colour(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Hex Colour Invalid'


def test_help(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock()

    help(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (
        'CSUIBot v0.0.1\n\n'
        'Dari Fasilkom, oleh Fasilkom, untuk Fasilkom!'
    )
    assert args[1] == expected_text


def test_zodiac(mocker):
    fake_zodiac = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_zodiac', return_value=fake_zodiac)
    mock_message = Mock(text='/zodiac 2015-05-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_zodiac


def test_zodiac_invalid_month_or_day(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/colour 2015-25-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Month or day is invalid'


def test_shio(mocker):
    fake_shio = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_chinese_zodiac', return_value=fake_shio)
    mock_message = Mock(text='/shio 2015-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_shio


def test_shio_invalid_year(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_chinese_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/shio 1134-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Year is invalid'
